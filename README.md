# Happy Map Reduce Example

This project illustrates a simple MapReduce solution using Python and the World Happiness database from Kaggle.

## Project 

For 44-564 Big Data
Spring 2018
Northwest Missouri State University

Denise Case
dcase@nwmissouri.edu

## Links

## Introduction

## The Data

## The Challenge

## The Question

## The Solution

## Results

## References & Resources



